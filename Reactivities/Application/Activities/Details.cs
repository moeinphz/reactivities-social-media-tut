using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using MediatR;
using Persistence;

namespace Application.Activities {
    // Query
    public class Details {
        // The request that comes into the application
        public class Query : IRequest<Activity> {
            public Guid Id { get; set; }
        }

        // Handles the request nad it returns an activity
        public class Handler : IRequestHandler<Query, Activity> {
            private readonly DataContext _context;
            public Handler (DataContext context) {
                _context = context;
            }

            public async Task<Activity> Handle (Query request, CancellationToken cancellationToken) {
                var activity = await _context.Activities.FindAsync(request.Id);

                if(activity == null) 
                    throw new RestException(HttpStatusCode.NotFound, new {activity = "Not found"});

                return activity;
            }
        }
    }
}