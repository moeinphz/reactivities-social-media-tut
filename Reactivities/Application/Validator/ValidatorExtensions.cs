using FluentValidation;

namespace Application.Validator
{
    public static class ValidatorExtensions
    {
        public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder) 
        {
            var options = ruleBuilder
                .NotEmpty()
                .MinimumLength(6)
                .WithMessage("Password must be at least 6 charcaters").Matches("[A-Z]")
                .WithMessage("Password must contain 1 UPPERCASE letter")
                .Matches("[a-z]").WithMessage("Password must have at least 1 lowercase character")
                .Matches("[0-9]").WithMessage("Password must have at least a number")
                .Matches("[^a-zA-Z0-9]")
                .WithMessage("Password must have at non alpha numeric");
            return options;
        }
    }
}