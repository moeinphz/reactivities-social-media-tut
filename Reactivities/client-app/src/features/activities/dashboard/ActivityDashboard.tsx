import React, {useContext, useEffect} from 'react'
import { Grid } from 'semantic-ui-react'
// import ActivityDetails from '../details/ActivityDetails'
// import ActivityForm from '../form/ActivityForm'
import {observer} from "mobx-react-lite";
// import ActivityStore from "../../../app/stores/activityStore";
import ActivityList from "./ActivityList";
import {RootStoreContext} from "../../../app/stores/rootStore";
import LoadingComponent from "../../../app/layouts/LoadingComponent";



const ActivityDashboard: React.FC = observer(() => {

    // const activityStore = useContext(ActivityStore);
    // const {editMode, activity} = activityStore;

    const rootStore = useContext(RootStoreContext);
    const {loadActivities, loadingInitial} = rootStore.activityStore;

    useEffect(() => {
        loadActivities();
    }, [loadActivities]);

    if (loadingInitial)
        return <LoadingComponent content='Loading activities'/>

    return (
        <Grid>
            <Grid.Column width={10}>
                <ActivityList />
            </Grid.Column>
            {/*<Grid.Column width={6}>*/}
                {/*{activity && !editMode &&*/}
                {/*<ActivityDetails />}*/}

                {/*{editMode && <ActivityForm*/}
                                {/*key={activity && (activity.id || 0)}*/}
                                {/*activity={activity!} />}*/}

            {/*</Grid.Column>*/}
            <h2>Activity Filters</h2>
        </Grid>
    )
});

export default ActivityDashboard;
