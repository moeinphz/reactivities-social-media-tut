import {action, computed, observable, runInAction} from "mobx";
import {SyntheticEvent} from "react";
import {IActivity} from "../models/activity";
import agent from "../api/agent";
import { history } from '../..';
import {toast} from "react-toastify";
import {RootStore} from "./rootStore";



export default class ActivityStore  {

    rootStore: RootStore;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    @observable activityRegistry = new Map();
    @observable activities: IActivity[] = [];
    @observable activity: IActivity | null = null;

    @observable loadingInitial = false;
    @observable editMode = false;
    @observable submitting = false;

    @observable target = '';


    @computed get activitiesByDate() {
        return this.groupActivitiesByDate(Array.from(this.activityRegistry.values()));
    }

    groupActivitiesByDate(activities: IActivity[]) {
        const sortedActivities = activities.sort(
            (a, b) => b.date.getTime() - a.date.getTime());
        return Object.entries(sortedActivities.reduce((activities, activity) => {
            const date = activity.date.toISOString().split('T')[0];
            activities[date] = activities[date] ? [...activities[date], activity] : [activity];
            return activities;
        }, {} as {[key: string]: IActivity[]}));
    }


    @action loadActivities = async () => {
        this.loadingInitial = true;

        try {
            const activities = await agent.Activities.list();
            runInAction('loadingActivities', () => {
                activities.forEach(activity => {
                    activity.date = new Date(activity.date);
                    this.activityRegistry.set(activity.id, activity);
                });
                this.loadingInitial = false;
            });

        } catch (e) {
            runInAction('loadingActivities error', () => {
                this.loadingInitial = false;
                console.log(e);
            });
        }
    };


    @action loadActivity = async (id: string) => {
        let activity = this.getActivity(id);
        if (activity) {
            this.activity = activity;
            return activity;
        } else {
            this.loadingInitial = true;
            try {
                activity = await agent.Activities.details(id);
                runInAction('getting Activity', () => {
                    activity.date = new Date(activity.date);
                    this.activity = activity;
                    this.activityRegistry.set(activity.id, activity);
                    this.loadingInitial = false;
                });
                return activity;
            } catch (e) {
                runInAction('getting Activity Error', () => {
                    this.loadingInitial = false;
                    console.log(e);
                });
                // throw e; // activityDetails will catch it
            }
        }
    };

    getActivity = (id: string) => {
        return this.activityRegistry.get(id);
    };

    @action clearActivity = () => {
        this.activity = null;
    };

    @action selectActivity = (id: string) => {
        this.activity = this.activityRegistry.get(id);
        this.editMode = false;
    };

    @action createActivity = async (activity: IActivity) => {
        this.submitting = true;
        try {
            await agent.Activities.create(activity);
            runInAction('creating Activity', () => {
                this.activityRegistry.set(activity.id, activity);
                this.editMode = false;
                this.submitting = false;
            });
            history.push(`/activities/${activity.id}`)

        } catch (e) {
            runInAction('creating Activity error', () => {
                this.submitting = false;
                console.log(e);
                toast.error('Problem submitting error')
            });
        }
    };

    @action openCreateForm = () => {
        this.editMode = true;
        this.activity = null;
    };

    @action openEditForm = (id: string) => {
        this.activity = this.activityRegistry.get(id);
        this.editMode = true;
    };

    @action cancelSelectedActivity = () => {
        this.activity = null;
    };

    @action editActivity = async (activity: IActivity) => {
        this.submitting = true;
        try {
            await agent.Activities.update(activity);
            runInAction('editting Activity', () => {
                this.activityRegistry.set(activity.id, activity);
                this.activity = activity;
                this.editMode = false;
                this.submitting = false;
            });
            history.push(`/activities/${activity.id}`)
        } catch (e) {
            runInAction('editting Activity error', () => {
                this.submitting = false;
                console.log(e);
            });

        }
    };

    @action cancelFormOpen = () => {
        this.editMode = false;
    };

    @action  deleteActivity = async (event: SyntheticEvent<HTMLButtonElement>, id: string) => {
        this.submitting = true;
        this.target = event.currentTarget.name;

        try {
            await agent.Activities.delete(id);
            runInAction('deleting Activity error', () => {
                this.activityRegistry.delete(id);
                this.submitting = false;
                this.target = '';
            });

        } catch (e) {
            runInAction('deleting Activity error', () => {
                this.submitting = false;
                this.target = '';
                console.log(e);
            });

        }
    };


}




// Promis Chain
// agent.Activities.list()
//     .then(activities => {
//         this.activities = [];
//         activities.forEach(activity => {
//             activity.date = activity.date.split('.')[0];
//             this.activities.push(activity)
//         });
//     })
//     .catch((error) => console.log(error))
//     .finally(() => this.loadingInitial = false);