import {RootStore} from "./rootStore";
import {action, observable, reaction} from "mobx";

export default class CommonStore {

    rootStore: RootStore;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;

        reaction( // it is like an observable for the token, whenever it is populated this will react on it
            () => this.token, // What is it that we wanna react on.

            token => {
                if (token) {
                    window.localStorage.setItem('jwt', token);
                } else {
                    window.localStorage.removeItem('jwt');
                }
            }
        )
    }

    @observable token: string | null = window.localStorage.getItem('jwt');
    @observable appLoaded = false;

    @action setToken = (token: string | null) => {
        // window.localStorage.setItem('jwt', token!);
        this.token = token;
    };

    @action setAppLoaded = () => {
        this.appLoaded = true;
    };
}