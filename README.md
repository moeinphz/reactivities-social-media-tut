# Reactivities Social Media Tut

It is a mini social media app written in ReactJs and Dotnet core 2.2

###### **Frontend** 
The client side of this project is developed using ReactJs Library. The visual side of this part uses Semantic UI and the state management is implemented MobX and MobX React Lite.

Semantic UI version: Latest;
	
	$  npm install semantic-ui-react semantic-ui-css
	
Mobx version: 5.9.0
Mobx-react.lite version: 1.0.1

	$  npm install mobx-react-lite@1.0.1 mobx@5.9.0
	
Backend calls and responses are handled using Axios: ^0.21.0

    $ npm install react-axios
    
and
    
    $ npm install axios
    $ npm install react
    $ npm install prop-types

also error handling on backend calls are tackled Axios Interceptor.

React Toastify library handles the Toast operations.
react-toastify: 6.1.0

    $ npm install --save react-toastify
    
React Final Form used for Form handling.

    // $ npm install react-final-form final-form
    
    $ npm install react-final-form@6.3.0 
    
React Widget to handle Date Picker:

    $ npm install react-widgets react-widgets-date-fns 
    $ npm install date-fns@^2.0.0

    
In order to get it working we need to create a folder called **typings-custom** then create a file called react-widgets-date-fns.d.ts containing:
    `declare module 'react-widgets-date-fns';`
then in _tsconfig.json_ :

    `"include": [
        "src",
        "./typings-custom/**/*.ts"
    ]`

    $ npm install  @types/react-widgets
    
Validating the form is performed using RELAVIDATE Library:
 
        $ npm install --save revalidate
        $ npm install @types/revalidate



###### **Backend**
The backend of this project has taken advantage of ASP.net Core 2.2 using CQRS and MediatR pattern.
The database is of type SQL Pattern and for testing purposes uses SQLite.

Validation is handled in the backened with **Fluent Validation Middleware**.

Authorisation is handled in the backend using:
    
    _`Microsoft.AspNetCore.Identity.EntityFrameworkCore`_


